<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employees extends Model
{
    //==================== show all employees ====================
    public static function ShowAll() {
        $data = DB::select("SELECT * FROM employees "); 
        return $data;
    }
    //add employee
    public static function add_employee ($name,$decription) {
        DB::insert('insert into employees (name, decription) values (?, ?)', [$name, $decription]);
    } 
    // ==================== edit employee ====================
    public static function edit_employee($id){
       $data = DB::select('select * from employees where id =?',[$id]);
       return $data;
    }
    public static function update_employee($name,$decription,$id) {
        DB::update('update employees 
        set 
            name = ?,
            decription = ? 
        where 
            id = ?', [$name,$decription,$id]);
    }
    // ==================== delete employee ====================
    public static function delete_employee($id){
        DB::delete('delete from employees where id = ?', [$id]);
    }
}
