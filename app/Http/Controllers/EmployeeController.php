<?php

namespace App\Http\Controllers;
use App\Employees;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    // ==================== fetch all employee ====================
    public function index()
    {
        $employees = Employees::ShowAll();
        return view('employees.index', ['employees' => $employees]);
    }
    // ==================== insert employee ====================
    public function create()
    {
       return view ('employees.create');
    }
    public function store()
    {
        $name = Request()->name;
        $decription = Request()->decription;
        Employees::add_employee($name,$decription);   
       return redirect('/employees')->with('success','Insert Successfully'); 
    }
    // ==================== edit employee ====================
    public function edit($id)
    {
       $employee = Employees::edit_employee($id);
        return view ('employees.edit',['employee' =>$employee]);
    }
    public function update($id)
    {
        $name = Request()->name;
        $decription = Request()->decription;
        Employees::update_employee($name,$decription,$id);
        return redirect('/employees')->with('success','Data update Successfully');
    }
    // ==================== delete employee ====================
    public function destroy($id)
    {
        Employees::delete_employee($id);
        return redirect('/employees')->with('success','Delete Successfully');
    }
}
