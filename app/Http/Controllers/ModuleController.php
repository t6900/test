<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Modules;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function index()
    {
        $employees = DB::table('employees')->get();
        return view('test', ['employees' => $employees]);
    }
}
