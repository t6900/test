<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// ==================== all employees ================
Route::get('/employees', 'EmployeeController@index');
// ==================== create data in table ====================
Route::get('/employees/create', 'EmployeeController@create');
Route::post('/employees', 'EmployeeController@store');
// ==================== edit data in table ====================
Route::get('/employees/{id}', 'EmployeeController@edit');
Route::post('/update/{id}', 'EmployeeController@update');
// ==================== delete data in table ====================
Route::get('/employees/{id}','EmployeeController@destroy');


