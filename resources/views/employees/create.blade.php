<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Employee</title>
</head>
<body>
    <h1>Create new Employee</h1>
    <form action="/employees" method="POST">
    @csrf
        <div class="container">
                <label class="form-label">Name</label>
                <input type="text"  class="form-control form-control-sm " name="name" >
                <label>Decription</label>
                <input type="text" class="form-control form-control-sm" name="decription">
                <br>
                <button type="submit" class="btn btn-primary">Submit</button
        </div>
     </form>
</body>
</html>