<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lucaburgio/iconoir@master/css/iconoir.css">
    <title>Document</title>
</head>
<body>
    <h1>Welcome to Turbo tech company</h1>
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    <hr>
    <div class="container">
        <a href="{{ url('employees/create') }}"><button type="button" class="btn btn-success">Add Employee <span><i class="iconoir-add-circled-outline"></i></span></button></a> 
        <table class="table">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Decription</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
            <tr>
            <th>{{$employee -> id}}</th>
            <td>{{$employee -> name}}</td>
            <td>{{$employee -> decription}}</td>
            <td>
                <a href="employees/{{$employee -> id}}" class="btn btn-primary">Edit <span><i class="iconoir-edit"></i></span></a>
            </td>
            <td>
                <a href="employees/{{$employee ->id}}" class="btn btn-danger">Delete<span><i class="iconoir-delete-circled-outline"></i></span></a>
            </tr> 
            @endforeach      
        </tbody>
        </table>
    </div>
</body>
</html>