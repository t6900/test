<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Edit Employee</title>
</head>
<body>
    <h1>Edit Employee</h1>
    <form action="/update/{{$employee[0] ->id}}" method="POST">
    @csrf
        <div class="container">
                <label class="form-label">Name</label>
                <input type="text"  class="form-control form-control-sm " name="name" value="{{$employee[0]->name}}">
                <label>Decription</label>
                <input type="text" class="form-control form-control-sm" name="decription" value="{{$employee[0]->decription}}">
                <br>
                <button type="submit" class="btn btn-primary">Update</button
        </div>
     </form>
</body>
</html>